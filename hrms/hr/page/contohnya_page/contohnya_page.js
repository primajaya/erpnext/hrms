frappe.pages['contohnya-page'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Contohnya page',
		single_column: true
	});
	// var parent = $('<div class="welcome-to-erpnext"></div>').appendTo(wrapper);
	var parent = $('.layout-main-section');
	console.log(wrapper);
	parent.html(frappe.render_template("contohnya_page", {"title":"Judul Cuy"}));
	let element = document.querySelector('.target');

	let datatabel = new DataTable(element, {
		columns: ['Framework', 'Built By', 'GitHub Stars', 'License', 'Contributors', 'Age','Project Home', 'Project Link'],
		data: [
			['React', 'Facebook', 149017, 'MIT', 1385, '7 Years', 'https://reactjs.org', 'https://github.com/facebook/react'],
			['Angular', 'Google', 61263, 'MIT', 1119, '5 Years', 'https://angular.io', 'https://github.com/angular/angular'],
			['Vue', 'Evan You', 164408, 'MIT', 293, '4 Years', 'https://vuejs.org', 'https://github.com/vuejs/vue'],
			['Svelte', 'Rich Harris', 33865, 'MIT', 298, '3 Years', 'https://svelte.dev', 'https://github.com/sveltejs/svelte/'],
			['Stencil', 'Ionic Team', 7749, 'MIT', 132, '3 Years', 'https://stenciljs.com', 'https://github.com/ionic-team/stencil'],
		]
	});
}


function submit3(){
	console.log($('[name="file_xlsx"]')[0].files[0]);
	var data = new FormData();
	data.append('file_xlsx',$('[name="file_xlsx"]')[0].files[0]);
	data.append('csrf_token',frappe.csrf_token);
	$.ajax({
		url: '/api/method/hrms.hr.page.contohnya_page.contohnya_page.handle2',
		data: data,
		cache: false,
		contentType: false,
		processData: false,
		method: 'POST',
		type: 'POST', // For jQuery < 1.9
		success: function(data){
			alert(data);
		}
	});
}

function submit2(){
	console.log($('[name="file_xlsx"]')[0].files[0]);
	var data = new FormData();
	// data.append('file_xlsx',$('[name="file_xlsx"]')[0].files[0]);
	data.append('csrf_token',frappe.csrf_token);
	frappe.call({
		type: "POST",
		method: "hrms.hr.page.contohnya_page.contohnya_page.handle3",
		// args: {
		// 	file_xlsx: $('[name="file_xlsx"]')[0].files[0],
		// 	// file_xlsx: data,
		// 	csrf_token:frappe.csrf_token
		// },
		args:{args:data},
		// args:data,
		// // Specify processData as false, so that jQuery doesn't process the data and doesn't try to serialize it
		// dataType: 'multipart/form-data',
		callback: function(r) {
			console.log(r);
			alert('data');
		},
	});

	submit3();
}


function openCustomDialog() {
    var d =  new frappe.ui.Dialog({
        title: __('Custom Dialog'),
        fields: [
            {
                fieldtype: 'HTML',
                options: frappe.render_template("contohnya_page", {"title":"Dialog Form Cuy"}),  // Replace with the correct path to your HTML file
                label: __('Custom HTML Content'),
            },
        ],
        primary_action: function () {
            // Your custom logic when the primary action button is clicked
            // For example, you can perform some validation or save data
            frappe.msgprint('Primary Action Clicked');
            frappe.ui.hide_dialog();
        },
        primary_action_label: __('Save'),
    });
	d.show();

	d.$wrapper.find('.modal-content').css("width", "800px");


}

// frappe.pages['welcome-to-erpnext'].on_page_load = function(wrapper) {
// 	var parent = $('<div class="welcome-to-erpnext"></div>').appendTo(wrapper);

// 	parent.html(frappe.render_template("welcome_to_erpnext", {}));

// 	parent.find(".video-placeholder").on("click", function() {
// 		window.erpnext_welcome_video_started = true;
// 		parent.find(".video-placeholder").addClass("hidden");
// 		parent.find(".embed-responsive").append('<iframe class="embed-responsive-item video-playlist" src="https://www.youtube.com/embed/videoseries?list=PL3lFfCEoMxvxDHtYyQFJeUYkWzQpXwFM9&color=white&autoplay=1&enablejsapi=1" allowfullscreen></iframe>')
// 	});

// 	// pause video on page change
// 	$(document).on("page-change", function() {
// 		if (window.erpnext_welcome_video_started && parent) {
// 			parent.find(".video-playlist").each(function() {
// 				this.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
// 			});
// 		}
// 	});
// }
